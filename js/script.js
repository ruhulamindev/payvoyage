const $ = $el => document.querySelector($el);

const menuOpen = document.querySelector("#main-menu-open");
const menuClose = document.querySelector("#main-menu-close");
const mobileMenu = document.querySelector("#mobile-menu");

menuOpen.addEventListener("click", _ => {
  mobileMenu.classList.add("open");
});

menuClose.addEventListener("click", _ => {
  mobileMenu.classList.remove("open");
});
